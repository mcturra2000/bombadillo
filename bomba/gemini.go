package bomba

import (
	"bytes"
	"crypto/sha1"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// FN Gemini 
type Gemini struct {
	url Url
	certs *TofuDigest
	strict bool
}

// How we express the content we get back from the server 
//type RawContentType string 

// FN Capsule 
type Capsule struct {
	// the following fields are filled out by GeminiVist:
	ClientReq string // what we send to the server to make a request
	ServerStat string // what server says back as response (not body)
	Status  int
	MimeMaj string
	MimeMin string
	Content string// body in raw form from the server
	Links   []string

}

type TofuDigest struct {
	certs      map[string]string
}

var BlockBehavior string = "block"
var TlsTimeout time.Duration = time.Duration(15) * time.Second

//------------------------------------------------\\
// + + +          R E C E I V E R S          + + + \\
//--------------------------------------------------\\

func (t *TofuDigest) Purge(host string) error {
	host = strings.ToLower(host)
	if host == "*" {
		t.certs = make(map[string]string)
		return nil
	} else if _, ok := t.certs[strings.ToLower(host)]; ok {
		delete(t.certs, host)
		return nil
	}
	return fmt.Errorf("Invalid host %q", host)
}

func (t *TofuDigest) Add(host, hash string, time int64) {
	t.certs[strings.ToLower(host)] = fmt.Sprintf("%s|%d", hash, time)
}

func (t *TofuDigest) Exists(host string) bool {
	if _, ok := t.certs[strings.ToLower(host)]; ok {
		return true
	}
	return false
}

func (t *TofuDigest) Find(host string) (string, error) {
	if hash, ok := t.certs[strings.ToLower(host)]; ok {
		return hash, nil
	}
	return "", fmt.Errorf("Invalid hostname, no key saved")
}

func (t *TofuDigest) Match(host, localCert string, cState *tls.ConnectionState) error {
	now := time.Now()

	for _, cert := range cState.PeerCertificates {
		if localCert != hashCert(cert.Raw) {
			continue
		}

		if now.Before(cert.NotBefore) {
			return fmt.Errorf("Certificate is not valid yet")
		}

		if now.After(cert.NotAfter) {
			return fmt.Errorf("EXP")
		}

		if err := cert.VerifyHostname(host); err != nil && cert.Subject.CommonName != host {
			return fmt.Errorf("Certificate error: %s", err)
		}

		return nil
	}

	return fmt.Errorf("No matching certificate was found for host %q", host)
}

func (t *TofuDigest) newCert(host string, cState *tls.ConnectionState) error {
	host = strings.ToLower(host)
	now := time.Now()
	var reasons strings.Builder

	for index, cert := range cState.PeerCertificates {
		if index > 0 {
			reasons.WriteString("; ")
		}
		if now.Before(cert.NotBefore) {
			reasons.WriteString(fmt.Sprintf("Cert [%d] is not valid yet", index+1))
			continue
		}

		if now.After(cert.NotAfter) {
			reasons.WriteString(fmt.Sprintf("Cert [%d] is expired", index+1))
			continue
		}

		if err := cert.VerifyHostname(host); err != nil && cert.Subject.CommonName != host {
			reasons.WriteString(fmt.Sprintf("Cert [%d] hostname does not match", index+1))
			continue
		}

		t.Add(host, hashCert(cert.Raw), cert.NotAfter.Unix())
		return nil
	}

	return fmt.Errorf(reasons.String())
}

func (t *TofuDigest) GetCertAndTimestamp(host string) (string, int64, error) {
	certTs, err := t.Find(host)
	if err != nil {
		return "", -1, err
	}
	certTsSplit := strings.SplitN(certTs, "|", -1)
	if len(certTsSplit) < 2 {
		_ = t.Purge(host)
		return certTsSplit[0], -1, fmt.Errorf("Invalid certstring, no delimiter")
	}
	ts, err := strconv.ParseInt(certTsSplit[1], 10, 64)
	if err != nil {
		_ = t.Purge(host)
		return certTsSplit[0], -1, err
	}
	now := time.Now()
	if ts < now.Unix() {
		// Ignore error return here since an error would indicate
		// the host does not exist and we have already checked for
		// that and the desired outcome of the action is that the
		// host will no longer exist, so we are good either way
		_ = t.Purge(host)
		return "", -1, fmt.Errorf("Expired cert")
	}
	return certTsSplit[0], ts, nil
}

func (t *TofuDigest) IniDump() string {
	if len(t.certs) < 1 {
		return ""
	}
	var out strings.Builder
	out.WriteString("[CERTS]\n")
	for k, v := range t.certs {
		out.WriteString(k)
		out.WriteString("=")
		out.WriteString(v)
		out.WriteString("\n")
	}
	return out.String()
}

//------------------------------------------------\\
// + + +          F U N C T I O N S          + + + \\
//--------------------------------------------------\\

// FN _geminiRetrieve 
// strict => we must validate certificates, otheriwse allow
func _geminiRetrieve(g Gemini) (string, error, string) {
	logit("geminiRetrieve")
	host := g.url.Host
	port := g.url.Port
	certs := g.certs
	if host == "" || port == "" {
		return "", fmt.Errorf("Incomplete request url"), ""
	}

	addr := host + ":" + port

	conf := &tls.Config{
		MinVersion:         tls.VersionTLS12,
		InsecureSkipVerify: true,
	}

	conn, err := tls.DialWithDialer(&net.Dialer{Timeout: TlsTimeout}, "tcp", addr, conf)
	if err != nil {
		return "", fmt.Errorf("TLS Dial Error: %s", err.Error()), ""
	}

	defer conn.Close()

	if g.strict {
		connState := conn.ConnectionState()

		// Begin TOFU screening...

		// If no certificates are offered, bail out
		if len(connState.PeerCertificates) < 1 {
			return "", fmt.Errorf("Insecure, no certificates offered by server"), ""
		}

		host := g.url.Host

		localCert, localTs, err := certs.GetCertAndTimestamp(host)

		if localTs > 0 {
			// See if we have a matching cert
			err := certs.Match(host, localCert, &connState)
			if err != nil && err.Error() != "EXP" {
				// If there is no match and it isnt because of an expiration
				// just return the error
				return "", err , ""
			} else if err != nil {
				// The cert expired, see if they are offering one that is valid...
				err := certs.newCert(host, &connState)
				if err != nil {
					// If there are no valid certs to offer, let the client know
					return "", err, ""
				}
			}
		} else {
			err = certs.newCert(host, &connState)
			if err != nil {
				// If there are no valid certs to offer, let the client know
				return "", err, ""
			}
		}
	}

	req := "gemini://" + addr + "/" + g.url.Resource
	send := req +"\r\n"

	_, err = conn.Write([]byte(send))
	if err != nil {
		return "", err, req
	}

	result, err := ioutil.ReadAll(conn)
	if err != nil {
		return "", err, req
	}

	return string(result), nil, req
}

/*
func (g Gemini) FetchXXX() (res Response) {
	logit("Fetch")

	res = Response{} // Response{make([]byte, 0), err}

	oops := func(msg string) (Response) {
		res.Err = fmt.Errorf(msg)
		return  res
	}
	rawResp, err, clientReq	 := geminiRetrieve(g)
	res.ClientReq = clientReq
	if err != nil {
		return oops(err.Error())
	}


	resp := strings.SplitN(rawResp, "\r\n", 2)
	res.ServerStat = resp[0]
	if len(resp) != 2 {
		if err != nil {
			return oops("Invalid response from server")
		}
	}
	header := strings.SplitN(resp[0], " ", 2)
	if len([]rune(header[0])) != 2 {
		header = strings.SplitN(resp[0], "\t", 2)
		if len([]rune(header[0])) != 2 {
			return oops("Invalid response format from server")
		}
	}

	// Get status code single digit form
	status, err := strconv.Atoi(string(header[0][0]))
	if err != nil {
		return oops("Invalid status response from server")
	}

	switch status {
	case 1:
		return oops("[1] Queries cannot be saved.")
	case 2:
		res.File = []byte(resp[1]);
		return res;
	case 3:
		return oops("[3] Redirects cannot be saved.")
	case 4:
		return oops("[4] Temporary Failure.")
	case 5:
		return oops("[5] Permanent Failure.")
	case 6:
		return oops("[6] Client Certificate Required (Unsupported)")
	default:
		return oops("Invalid response status from server")
	}

	//	return 

}
*/

// security be damned!
func GeminiVisitRelaxed(url Url) (Capsule, error) {
	certs := MakeTofuDigest()
	return GeminiVisit(url, &certs, false)
}

// FN GeminiVisit 
func GeminiVisit(url Url, certs *TofuDigest, strict bool) (Capsule, error) {
	//logit("GeminiVisit")
	capsule := MakeCapsule()
	g := Gemini{url, certs, strict}
	rawResp, err, clientReq := _geminiRetrieve(g)
	capsule.ClientReq = clientReq
	resource := url.Resource
	//resource.ClientReq = clientReq
	if err != nil {
		return capsule, err
	}

	resp := strings.SplitN(rawResp, "\r\n", 2)
	//fmt.Printf("*** client req: %s\n", clientReq);
	if len(resp) != 2 {
		if err != nil {
			return capsule, fmt.Errorf("Invalid response from server")
		}
	}
	capsule.ServerStat = resp[0]
	header := strings.SplitN(resp[0], " ", 2)
	//fmt.Printf("*** hdr='%s'\n", header)
	if len([]rune(header[0])) != 2 {
		header = strings.SplitN(resp[0], "\t", 2)
		if len([]rune(header[0])) != 2 {
			return capsule, fmt.Errorf("Invalid response format from server")
		}
	}

	body := resp[1]

	// Get status code single digit form
	capsule.Status, err = strconv.Atoi(string(header[0][0]))
	if err != nil {
		return capsule, fmt.Errorf("Invalid status response from server")
	}

	// Parse the meta as needed
	var meta string

	switch capsule.Status {
	case 1:
		capsule.Content = header[1]
		return capsule, nil
	case 2:
		mimeAndCharset := strings.Split(header[1], ";")
		meta = mimeAndCharset[0]
		if meta == "" {
			meta = "text/gemini"
		}
		minMajMime := strings.Split(meta, "/")
		if len(minMajMime) < 2 {
			return capsule, fmt.Errorf("Improperly formatted mimetype received from server")
		}
		capsule.MimeMaj = minMajMime[0]
		capsule.MimeMin = minMajMime[1]
		if capsule.MimeMaj == "text" && capsule.MimeMin == "gemini" {
			if len(resource) > 0 && resource[0] != '/' {
				resource = fmt.Sprintf("/%s", resource)
			} else if resource == "" {
				resource = "/"
			}
			u := g.url
			currentUrl := fmt.Sprintf("gemini://%s:%s%s", u.Host, u.Port, resource)
			capsule.Content, capsule.Links = parseGemini(body, currentUrl)
		} else {
			capsule.Content = body
		}
		return capsule, nil
	case 3:
		// The client will handle informing the user of a redirect
		// and then request the new url
		capsule.Content = header[1]
		return capsule, nil
	case 4:
		return capsule, fmt.Errorf("[4] Temporary Failure. %s", header[1])
	case 5:
		return capsule, fmt.Errorf("[5] Permanent Failure. %s", header[1])
	case 6:
		return capsule, fmt.Errorf("[6] Client Certificate Required (Unsupported)")
	default:
		return capsule, fmt.Errorf("Invalid response status from server")
	}
}

// mcarter added 2025-01-24		
func affixSchemeIcon(link string, decorator *string) {
	// mcarter 2025-01-24
	var has = func(scheme string) bool {
		return strings.Contains(link, scheme)
	}

	var icon = "🧩" // don't know initially
	if has("gemini:") {
		icon =  "♊"
	} else if has("http:") || has("https:") {
		icon =  "🌐"
	} else if has("spartan:") {
		icon = "💪"
	} else if has("gopher:") {
		icon = "🦫"
	}

	*decorator = icon + *decorator

}

func parseGemini(b, currentUrl string) (string, []string) {
	splitContent := strings.Split(b, "\n")
	links := make([]string, 0, 10)

	inPreBlock := false
	spacer := "      "

	outputIndex := 0
	for i, ln := range splitContent {
		splitContent[i] = strings.Trim(ln, "\r\n")
		isPreBlockDeclaration := strings.HasPrefix(ln, "```")
		if isPreBlockDeclaration && !inPreBlock && (BlockBehavior == "both" || BlockBehavior == "alt") {
			inPreBlock = !inPreBlock
			alt := strings.TrimSpace(ln)
			if len(alt) > 3 {
				alt = strings.TrimSpace(alt[3:])
				splitContent[outputIndex] = fmt.Sprintf("%s[ALT][ %s ]", spacer, alt)
				outputIndex++
			}
		} else if isPreBlockDeclaration {
			inPreBlock = !inPreBlock
		} else if len([]rune(ln)) > 3 && ln[:2] == "=>" && !inPreBlock {
			var link, decorator string
			subLn := strings.Trim(ln[2:], "\r\n\t \a")
			splitPoint := strings.IndexAny(subLn, " \t")

			if splitPoint < 0 || len([]rune(subLn))-1 <= splitPoint {
				link = subLn
				decorator = subLn
			} else {
				link = strings.Trim(subLn[:splitPoint], "\t\n\r \a")
				decorator = strings.Trim(subLn[splitPoint:], "\t\n\r \a")
			}

			if strings.Index(link, "://") < 0 {
			link, _ = GeminiHandleRelativeUrl(link, currentUrl)
		}

		links = append(links, link)
		linknum := fmt.Sprintf("[%d]", len(links))
		affixSchemeIcon(link, &decorator) // mcarter 2025-01-24
		splitContent[outputIndex] = fmt.Sprintf("%-5s %s", linknum, decorator)
		outputIndex++
	} else {
		if inPreBlock && (BlockBehavior == "alt" || BlockBehavior == "neither") {
			continue
		}
		var leader, tail string = "", ""
		/*
		if len(ln) > 0 && ln[0] == '#' {
			leader = "\033[1m"
			tail = "\033[0m"
		}
		*/
		splitContent[outputIndex] = fmt.Sprintf("%s%s%s%s", spacer, leader, ln, tail)
		outputIndex++
	}
}
return strings.Join(splitContent[:outputIndex], "\n"), links
}

// handleRelativeUrl provides link completion
func GeminiHandleRelativeUrl(relLink, current string) (string, error) {
	base, err := url.Parse(current)
	if err != nil {
		return relLink, err
	}
	rel, err := url.Parse(relLink)
	if err != nil {
		return relLink, err
	}
	return base.ResolveReference(rel).String(), nil
}

func hashCert(cert []byte) string {
	hash := sha1.Sum(cert)
	hex := make([][]byte, len(hash))
	for i, data := range hash {
		hex[i] = []byte(fmt.Sprintf("%02X", data))
	}
	return fmt.Sprintf("%s", string(bytes.Join(hex, []byte(":"))))
}

func MakeCapsule() Capsule {
	caps := Capsule{}
	caps.Status = -1
	return caps

	//return Capsule{"", "", 0, "", make([]string, 0, 5) , "", ""}
}

func MakeTofuDigest() TofuDigest {
	return TofuDigest{make(map[string]string)}
}
