package bomba

import (
	//"fmt"
	//"log"
	"strings"
	"time"

	"github.com/famz/SetLocale"
	gc "github.com/blippy/goncurses"

)

var screen strings.Builder

func MakeScreen() {
	//screen = strings.Builder
	wBody.Erase()
}

// currently used by bookmarks, which is a bad idea
func screenWriteString (str string) { /* screen.WriteString(str) */ }

func advanceLine () { /* screenWriteString("\r\n") */}
func esc (seq string) { /*screenWriteString("\033[" + seq)*/ }
func screenResetStyle ()  { esc("0m")}
func screenInvert () { esc("7m") }


// FN bodyln 
var isBold = false 
func bodyln (s string) {
	for _, c := range s {
		// Highlighting seatched text. See also markupFoundText
		if c == '\x15' { 
			if isBold {
				wBody.AttrOff(gc.AttrReverse)
				//isBold = false
				//wBody.Print(">>")
			} else {
				wBody.AttrOn(gc.AttrReverse)
				//isBold = true
				//wBody.Print("<<")
			}
			isBold = !isBold
		} else {
			wBody.Print(string(c))
		}
	}

	wBody.Println("")
	wBody.Refresh()
}

func screenSetFooter(s string) {
	wFtr.Erase()
        wFtr.Print(s)
	wFtr.Refresh()
	
}
func screenSetHeader(s string) {
	wHdr.Erase()
        wHdr.Print("💣 "+ s)
	wHdr.Refresh()
	
}

func screenWidth() int {
	_,w :=  wHdr.GetMaxYX()
	return w
}
func Wait(s time.Duration) {
	time.Sleep(s * time.Second)
}

// FN screenOutput
// TODO unnecessary
func screenOutput() {
	return
}


var scr *gc.Window // no-one else whoudl probably wirte to this
var wHdr *gc.Window // where the header/title goes
var wBody *gc.Window // where screen contents go
var wFtr *gc.Window // where the footer status line goes
var wCmd *gc.Window // where the user enters commands

var cpBoW gc.ColorPair // black on white

func Win(nlines, ncols, begin_y, begin_x int) *gc.Window {
	return gc.SubWin(scr, nlines, ncols, begin_y, begin_x)
	// NewWin
}

// FN uiInit 
func uiInit () {
	//var err error 
	SetLocale.SetLocale(SetLocale.LC_ALL, "") // needed for UTF-8 to work
	scr  = gc.Init()
	cpBoW = gc.ColorPair(1)
	cpBoW.Init(gc.ColorBlack, gc.ColorWhite)
	//cpBoW.Init(gc.ColorWhite, gc.ColorBlack)

	h, w := scr.GetMaxYX()
	wHdr = Win(1, w, 0, 0)
	wHdr.SetBackground("                                                                                                 ", gc.AttrNormal, cpBoW)
	screenSetHeader("Bombadillo")

	wBody = Win(h-3, w, 1, 0)

	wFtr = Win(1, w, h-2 , 0)
	wFtr.SetBackground("                                                                                                 ", gc.AttrNormal, cpBoW)
	screenSetFooter("Idle")

	wCmd = Win(1, w, h-1, 0)
	//wCmd.Print("this is the command ares")
	//Wait(10)

}

func uiDeinit() {
	gc.EndWin()
}

