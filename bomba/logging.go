package bomba

import (
	"fmt"
	"os"
	"strconv"
)

var logfile *os.File

var count = 1

func loggingInit() {
	os.Mkdir("/home/pi/.blippy", os.ModePerm) // TODO generalise
	var err error
	logfile, err = os.OpenFile("/home/pi/.blippy/bombadillo.log", 
	os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	logit("=== Started")
}

//func logit(str string) {
func logit(arg ...interface{}) {
	//logfile.WriteString(string(count))
	logfile.WriteString(strconv.Itoa(count) + " ")
	for i := range arg {
		b := []byte{}
		b = fmt.Append(b, arg[i])
		logfile.Write(b)
		logfile.WriteString(" ")
	}
	logfile.WriteString("\n")
	//	+ " " + str + "\n")
	count = count + 1
	logfile.Sync()
}

func logpanic(s string) {
	logit(s)
	panic(s)
}
func loggingDeinit() {
	logfile.Close()
}
