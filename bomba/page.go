package bomba

import (
	"fmt"
	"strings"
	//"tildegit.org/sloum/bombadillo/tdiv"
	//"gitlab.com/mcturra2000/bombadillo/bomba/
	"gitlab.com/mcturra2000/bombadillo/bomba/tdiv"
	//"bombadillo/tdiv"
	//"unicode"
)

//------------------------------------------------\\
// + + +             T Y P E S               + + + \\
//--------------------------------------------------\\

// Page represents a visited URL's contents; including
// the raw content, wrapped content, link slice, URL,
// and the current scroll position
type Page struct {
	WrappedContent []string
	RawContent     string
	Links          []string
	Location       Url
	ScrollPosition int
	FoundLinkLines []int
	SearchTerm     string
	SearchIndex    int
	FileType       string
	WrapWidth      int
	Color          bool
}

//------------------------------------------------\\
// + + +           R E C E I V E R S         + + + \\
//--------------------------------------------------\\


func (p *Page) RenderImage(width int) {
	w := (width - 5) * 2
	if w > 300 {
		w = 300
	}
	p.WrappedContent = tdiv.Render([]byte(p.RawContent), w)
	p.WrapWidth = width
}

// experimental more comprehensible content wrapping
// Added 25/2
// CAVEAT Doesn't handle escape sequences
func glyphify(rawContent string) [][]rune {
	//logit("glyphify:")
	//runes := []rune()

	// sort out the spacings and line-endings
	runesIn := []rune(rawContent) 
	runes1 := []rune{}
	for _, ch := range runesIn {
		if ch == '\r' || ch == '\v' || ch == '\b' || ch == '\f' || ch == '\a' {
			continue
		}

		if ch == '\u0085' || ch == '\u2028' || ch == '\u2029' {
			ch = '\n'
		}
		runes1 = append(runes1, ch)
	}


	isSpacey := func (r rune) bool { return IsSpaceLike(r) || r == '\n' }
	glyphs := [][]rune{}
	for i := 0; i < len(runes1) ; i++ {
		if isSpacey(runes1[i]) {
			glyphs = append(glyphs, []rune{runes1[i]})
			continue
		}
		// we seem to have a chunk of something, so let's collect it
		glyph := []rune{}
		for ; i< len(runes1); i++ {
			if isSpacey(runes1[i]) {  i-- ; break }
			glyph = append(glyph, runes1[i])
		}		
		glyphs = append(glyphs, glyph)

	}


	//for _, g := range glyphs { logit(string(g)) }
	return glyphs


}


// Added 25/2
func IsSpaceLike(r rune) bool {
	return r == '\t' || r == ' '
}

// Added 25/2
func eatSpaces(i *int, glyphs [][]rune) {
	for ; *i < len(glyphs); *i++ {
		if !IsSpaceLike(glyphs[*i][0]) { break }
	}
}

// CAVEAT Assumes runes are 1 space wide (some are wider)
// CAVEAT Very long words cause some problems
// Added 25/2
func paginate (glyphs [][]rune, spacer string , width int) string {

	output := ""
	slen := len(spacer)
	col := 0
	for i := 0 ; i < len(glyphs); i++ {
		g := glyphs[i]
		glen := len(g)
		if  g[0] ==  '\t' {
			glen = 8-(col % 8)
		}

		if glen + col == width {
			output += string(g) +  "\n" + spacer
			col = slen
			i++
			eatSpaces(&i, glyphs)
			i--
			continue
		}

		if glen + col > width {
			output += "\n" + spacer + string(g)
			col = slen + glen

			eatSpaces(&i, glyphs)
			continue
		}

		output += string(g)
		col += glen
		if g[0] == '\n' {
			//output += spacer
			col = 0
		}
	}
	return output
}

// FN WrapContent 
// WrapContent performs a hard wrap to the requested
// width and updates the WrappedContent
// of the Page struct width a string slice
// of the wrapped data
func (p *Page) WrapContent (width, maxWidth int, color bool) {
	if p.FileType == "image" {
		p.RenderImage(width)
		return
	}

	glyphs := glyphify(p.RawContent) // 25/2 supposed to simplifty
	spacer := "      "
	width = min(width, maxWidth)
	formatted := paginate(glyphs, spacer, width)
	//logit(formated)

	/*
	counter := 0
	//spacer := ""
	var content strings.Builder
	var esc strings.Builder
	escape := false
	content.Grow(len(p.RawContent))
	*/

	/*
	if p.Location.Mime == "1" { // gopher document
		spacer = "           "
	} else if strings.HasSuffix(p.Location.Mime, "gemini") { //gemini document
		spacer = "      "
	}
	*/

	/*
	runeArr := []rune(p.RawContent)
	for i := 0; i < len(runeArr); i++ {
		ch := runeArr[i]
		if escape {
			if color {
				esc.WriteRune(ch)
			}
			if (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') {
				escape = false
				if ch == 'm' {
					content.WriteString(esc.String())
					esc.Reset()
				}
			}
			continue
		}
		if ch == '\n' || ch == '\u0085' || ch == '\u2028' || ch == '\u2029' {
			// x85 => Next Line (NEL)
			// x2028 => Line separator
			// x2029 => paragraph separator
			content.WriteRune('\n')
			counter = 0
		} else if ch == '\t' {
			if counter+4 < width {
				content.WriteString("    ")
				counter += 4
			} else {
				content.WriteRune('\n')
				counter = 0
			}
		} else if ch == '\r' || ch == '\v' || ch == '\b' || ch == '\f' || ch == '\a' {
			// Get rid of control characters we don't want
			continue
		} else if ch == 27 {
			if p.Location.Scheme == "local" {
				if counter+4 >= width {
					content.WriteRune('\n')
				}
				content.WriteString("\\033")
				continue
			}
			escape = true
			if color {
				esc.WriteRune(ch)
			}
			continue
		} else {
			wrapWord(&i, width, &counter, ch, &content, spacer, runeArr);
		}
	}


	p.WrappedContent = strings.Split(content.String(), "\n")
	*/
	p.WrappedContent = strings.Split(formatted, "\n")
	p.WrapWidth = width
	p.Color = color
	p.HighlightFoundText()
}




func (p *Page) HighlightFoundText() {
	if p.SearchTerm == "" {
		return
	}
	for i, ln := range p.WrappedContent {
		_, p.WrappedContent[i] = markupFoundText(ln, p.SearchTerm)
	}
}

// FN markupFoundText 
// See also bodyln()
func markupFoundText (contentLine string, searchTerm string) (bool, string) {
	ln := contentLine
	found := strings.Index(ln, searchTerm)
	if found < 0 {return false, ln}
	format := "\x15%s\x15" // TODO potentially messes up unicode
	/*
	if bombadillo.Options["theme"] == "inverse" {
		format = "\033[27m%s\033[7m"
	}
	*/
	ln = strings.Replace(ln, searchTerm, fmt.Sprintf(format, searchTerm), -1)
	return true, ln
	//logit("ln", ln)
}

func (p *Page) FindText() {
	p.FoundLinkLines = make([]int, 0, 10)
	s := p.SearchTerm
	p.SearchIndex = 0
	if s == "" { return }
	for i, ln := range p.WrappedContent {
		var found bool
		found,  p.WrappedContent[i] = markupFoundText(ln, s)
		if found {
			p.FoundLinkLines = append(p.FoundLinkLines, i)
		}
	}
}

//------------------------------------------------\\
// + + +          F U N C T I O N S          + + + \\
//--------------------------------------------------\\

// MakePage returns a Page struct with default values
func MakePage(url Url, content string, links []string) Page {
	p := Page{make([]string, 0), content, links, url, 0, make([]int, 0), "", 0, "", 40, false}
	return p
}

