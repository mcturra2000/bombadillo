GOCMD := go
BINS = bombadillo bo-fetch
#BINARY := bombadillo
#PREFIX := /usr/local
PREFIX := $(HOME)/.local
EXEC_PREFIX := ${PREFIX}
BINDIR := ${EXEC_PREFIX}/bin
SHARE := ${PREFIX}/share
MANDIR := ${PREFIX}/man/man1
ICONS := $(SHARE)/icons
APPS := $(SHARE)/applications
#MAN1DIR := ${MANDIR}/man1
test : GOCMD := go1.11.13
GOFILES = $(shell find bomba/ -type f -name '*.go') # all "library files"

# Use a dateformat rather than -I flag since OSX
# does not support -I. It also doesn't support 
# %:z - so settle for %z.
BUILD_TIME := ${shell date "+%Y-%m-%dT%H:%M%z"}

.PHONY: all
all: $(BINS)

clean :
	rm -f $(BINS) *.gz

bombadillo : bombadillo.go $(GOFILES)
	go build $< 

bo-fetch : bo-fetch.go $(GOFILES)
	go build $< 

info :
	@echo "Files are: $(GOFILES)"

.PHONY: install 
#install: install-bin install-man install-desktop clean
install: bombadillo bombadillo.1.gz bo-fetch.1.gz
	-rm -f $(BINDIR)/bombadillo
	ln bombadillo $(BINDIR)
	-rm -f $(BINDIR)/bo-fetch
	ln bo-fetch $(BINDIR)
	#install -d $(BINDIR)
	#install bombadillo $(BINDIR)
	#install bo-fetch $(BINDIR)

	install -d $(MANDIR)
	install bombadillo.1.gz  $(MANDIR)
	install bo-fetch.1.gz  $(MANDIR)

	install -d $(ICONS)
	install bombadillo-icon.png   $(ICONS)
	install -d $(APPS)
	install bombadillo.desktop $(APPS)


bo-fetch.1.gz : bo-fetch.1
	gzip -c ./$< > ./$@
	#gzip -c ./bombadillo.1 > ./bombadillo.1.gz

bombadillo.1.gz : bombadillo.1
	gzip -c ./$< > ./$@


#.PHONY: install-man
#install-man: bombadillo.1
#	gzip -c ./bombadillo.1 > ./bombadillo.1.gz
#	install -d ${DESTDIR}${MAN1DIR}
#	install -m 0644 ./bombadillo.1.gz ${DESTDIR}${MAN1DIR}

#.PHONY: install-desktop
#install-desktop:
#ifeq ($(shell uname), Linux)
#	# These steps will not work on Darwin, Plan9, or Windows
#	# They would likely work on BSD systems
#	install -d ${DESTDIR}${DATAROOTDIR}/applications
#	install -m 0644 ./bombadillo.desktop ${DESTDIR}${DATAROOTDIR}/applications
#	install -d ${DESTDIR}${DATAROOTDIR}/pixmaps
#	install -m 0644 ./bombadillo-icon.png ${DESTDIR}${DATAROOTDIR}/pixmaps
#	-update-desktop-database 2> /dev/null
#else
#	@echo "* Skipping protocol handler associations and desktop file creation for non-linux system *"
#endif

#.PHONY: install-bin
#install-bin: build
#	install -d ${DESTDIR}${BINDIR}
#	install -m 0755 ./${BINARY} ${DESTDIR}${BINDIR}

#.PHONY: clean
#clean: 
#	${GOCMD} clean
#	rm -f ./bombadillo.1.gz 2> /dev/null
#	rm -f ./${BINARY}_* 2> /dev/null

#.PHONY: uninstall
#ninstall: clean
#	rm -f ${DESTDIR}${MAN1DIR}/bombadillo.1.gz
#	rm -f ${DESTDIR}${BINDIR}/${BINARY}
#	rm -f ${DESTDIR}${DATAROOTDIR}/applications/bombadillo.desktop
#	rm -f ${DESTDIR}${DATAROOTDIR}/pixmaps/bombadillo-icon.png
#	-update-desktop-database 2> /dev/null

#.PHONY: release
#release:
#	GOOS=linux GOARCH=amd64 ${GOCMD} build ${LDFLAGS} -o ${BINARY}_linux_64
#	GOOS=linux GOARCH=arm ${GOCMD} build ${LDFLAGS} -o ${BINARY}_linux_arm
#	GOOS=linux GOARCH=386 ${GOCMD} build ${LDFLAGS} -o ${BINARY}_linux_32
#	GOOS=darwin GOARCH=amd64 ${GOCMD} build ${LDFLAGS} -o ${BINARY}_darwin_64


#.PHONY: test
#test: clean build

