module gitlab.com/mcturra2000/bombadillo

//module bombadillo

go 1.23.2

require (
	github.com/blippy/goncurses v0.1.4
	github.com/famz/SetLocale v0.0.0-20140414113655-0457ad1065dd
	github.com/pkg/term v1.1.0
)

//github.com/blippy/goncurses v0.0.0-20250205202227-0eb59c241e9a // indirect
require golang.org/x/sys v0.15.0 // indirect
